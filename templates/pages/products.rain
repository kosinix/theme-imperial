{% if collection.filters.mode == 'grid' or template == 'pages/product.rain' or template == 'pages/index.rain' %}
    {% set perrow = theme.num_thumbnails_per_row %}
    {% if template == 'pages/index.rain' %}{% set perrow = theme.featured_products_per_row %}{% endif %}
    <div class="product product_griditem col-xs-6 col-sm-4 col-md-{{ 12 / perrow }}">
        <div class="inner">
            <a class="image" href="{{ product.url | url }}" title="{{ product.fulltitle }}">
                {% if product.image %}
                    {% if theme.product_image_fit %}
                        <img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ product.image | url_image('268x268x2', product.fulltitle) }}" width="268" height="268" alt="{{ product.fulltitle }}" title="{{ product.fulltitle }}" />
                    {% else %}
                        <img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ product.image | url_image('268x268x1', product.fulltitle) }}" width="268" height="268" alt="{{ product.fulltitle }}" title="{{ product.fulltitle }}" />
                    {% endif %}
                {% else %}
                    <img src="{{ 'pro-icon.png' | url_asset }}" alt="{{ product.title }}" title="{{ product.title }}"/>
                {% endif %}
            </a>

            <span class="name">
                {% if product.fulltitle %}
                    {{ product.fulltitle }}
                {% else %}
                    {{ product.title }}
                {% endif %}
            </span>

            <span class="price">
                {% if product.price.price_old %}
                    {{ product.price.price_old | money }}
                {% elseif product.price.price %}
                    {{ product.price.price | money }}
                {% endif %}
            </span>

            <a href="{{ ('cart/add/' ~ product.vid ) | url }}" class="cart">{{ 'Add to Cart' | t }}</a>
        </div>
    </div>
    
    {% if loop.index is divisibleby(perrow) and template == 'pages/product.rain' %}
    <div class="clearfix visible-xs"></div>
    {% endif %}
{% else %}
    <div class="product product_listitem">
        <div class="image">
            <a href="{{ product.url | url }}" title="{{ product.fulltitle }}">
                {% if template == 'pages/index.rain' and not product.price.price_old %}
                {% for id in newest %}
                {% if product.id == id.id %}
                <div class="sale-new new-label">
                    {{ 'New' | t }}
                </div>
                {% elseif product.price.price_old %}
                <div class="sale-new">
                    {{ 'Sale' | t }}
                </div>
                {% endif %}
                {% endfor %}
                {% elseif product.price.price_old %}
                <div class="sale-new">
                    {{ 'Sale' | t }}
                </div>
                {% endif %}
                {% if product.image %}
                {% if theme.product_image_fit %}
                <!-- fit -->
                <img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ product.image | url_image('268x268x1', product.fulltitle) }}" width="268" height="268" alt="{{ product.fulltitle }}" title="{{ product.fulltitle }}" />
                {% else %}
                <img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ product.image | url_image('268x268x2', product.fulltitle) }}" width="268" height="268" alt="{{ product.fulltitle }}" title="{{ product.fulltitle }}" />
                {% endif %}
                {% else %}
                <img src="{{ 'pro-icon.png' | url_asset }}" alt="{{ product.title }}" title="{{ product.title }}"/>
                {% endif %}
            </a>
        </div>
        <div class="details">
            <a href="{{ product.url | url }}" title="{{ product.fulltitle }}">
                <div title="{{ product.fulltitle }}" class="title label">
                    {% if theme.show_fulltitle and product.fulltitle %} {{ product.fulltitle }} {% else %}
                    {{ product.title }} {% endif %}
                </div>

                <div class="pricing {% if theme.setting_reviews and product.score %}pricingStars{% endif %}">
                    {% if product.price.price_old %}
                    <div class="left">
                        <span class="old-price">{{ product.price.price_old | money }}</span>
                        {% if shop.settings.legal.mode == 'strict' %}
                        <small class="srp">{{ 'SRP' | t }}</small>
                        {% endif %}
                    </div>
                    {% endif %}

                    {% if product.price.price_old %}
                    <div class="right">
                        {% endif %}
                        {{ product.price.price | money }}
                        {% if product.price.price_old %}
                    </div>
                    {% endif %}

                    {% if shop.settings.legal.mode == 'strict' %} <small>&#42; {{ 'Inc. tax Excl. Shipping Costs' | t }}</small>{% endif %}

                    {% if shop.settings.legal.mode == 'strict' and product.unit %}
                    <small class="unit-price">{{ 'Unit price' | t }}: {{ product.unit.price | money }} /
                        {{ product.unit.unit }}
                    </small>
                    {% endif %}
                </div>

                {% if theme.setting_reviews and product.score %}
                {% set productScore = product.score * 100 | number_format(0) %}
                <div class="stars">
                    <div style="width:{{ productScore }}%;"></div>
                </div>
                {% endif %}
            </a>

            {% if product.description %}<div class="description">{{ product.description }}</div>{% endif %}

            <a href="{{ ('cart/add/' ~ product.vid ) | url }}" class="cart btn">{{ 'Add to Cart' | t }}</a>
            <a href="{{ ('account/wishlistAdd/' ~ product.vid ) | url }}" class="wishlist">{{ 'Wishlist' | t }}</a>
        </div>
    </div>
{% endif %}