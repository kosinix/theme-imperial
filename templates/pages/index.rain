</div>
<div id="homepage-subhero" class="section">
    <div class="img"><img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ 'homepage-1.jpg' | url_asset }}" title="{{ theme.homepage_subhero_title | t }}" alt="{{ theme.homepage_subhero_title | t }}" /></div>
    <div class="info">
        <h1>{{ theme.homepage_subhero_title | t }}</h1>
        <h2>{{ theme.homepage_subhero_subtitle | t }}</h2>
        <a href="{{ theme.homepage_subhero_link }}" class="light-btn">{{ 'Learn More' | t }}</a>
    </div>
</div>

<div id="daily-deal" class="section">
    <h2>{{ 'Our Deals' | t }}</h2>
    <div id="deals">
        <div id="deal-1" class="deal">
            <a href="{{ theme.daily_deal_1_link }}">
                <div class="info">
                    <h3>{{ theme.daily_deal_1_title | t }}</h3>
                    <h4>{{ theme.daily_deal_1_subtitle | t }}</h4>
                    <div class="price">{{ theme.daily_deal_1_price | money_with_currency }}</div>
                </div>
                <div class="img"><img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ 'daily-deal.png' | url_asset }}" title="{{ theme.daily_deal_1_subtitle | t }}" alt="{{ theme.daily_deal_1_subtitle | t }}" /></div>
            </a>
        </div>
        <div id="deals-right">
            <div id="deal-2" class="deal">
                <a href="{{ theme.daily_deal_2_link }}">
                    <div class="info">
                        <h3>{{ theme.daily_deal_2_title | t }}</h3>
                        <h4>{{ theme.daily_deal_2_subtitle | t }}</h4>
                        <div class="price">{{ theme.daily_deal_2_price | money_with_currency }}</div>
                    </div>
                    <div class="img"><img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ 'daily-deal-2.png' | url_asset }}" title="{{ theme.daily_deal_2_subtitle | t }}" alt="{{ theme.daily_deal_2_subtitle | t }}" /></div>
                </a>
            </div>
            <div id="deal-3" class="deal">
                <a href="{{ theme.daily_deal_3_link }}">
                    <div class="info">
                        <h3>{{ theme.daily_deal_3_title | t }}</h3>
                        <h4>{{ theme.daily_deal_3_subtitle | t }}</h4>
                        <div class="price">{{ theme.daily_deal_3_price | money_with_currency }}</div>
                    </div>
                    <div class="img"><img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ 'daily-deal-3.png' | url_asset }}" title="{{ theme.daily_deal_3_subtitle | t }}" alt="{{ theme.daily_deal_3_subtitle | t }}" /></div>
                </a>
            </div>
        </div>
    </div>
</div>

{% if featured %}
<div id="featured_section" class="section">
    <div class="container">
        <h2>{{ 'Featured' | t }}</h2>
        <div class="products row ">
            {% for product in featured %}
                {% include 'snippets/products.rain' %}
            {% endfor %}
        </div>
    </div>
</div>
{% endif %}

{% if theme.history_title %}
    <div id="history" class="section">
        <h2>{{ theme.history_title | t }}</h2>
        <div class="container">
            <div class="col-md-6">
                <p class="subtitle">{{ theme.history_subtitle | t }}</p>
                <p class="years">{{ theme.history_years | t }}</p>
                <p class="subsubtitle">{{ theme.history_subsubtitle | t }}</p>
                <p class="description">{{ theme.history_description | t }}</p>
            </div>
            <div class="col-md-6">
                <div id="history-image">
                    <a href="{{ theme.history_link }}">
                        <img  class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ ('history1.jpg') | url_asset }}" />
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17px" height="40px" viewBox="0 0 17 40" style="enable-background:new 0 0 17 40;" xml:space="preserve"><polygon points="0,40 0,35.3 13.1,20 0,4.7 0,0 17,20 "/></svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
{% endif %}

{% if shop.settings.legal.mode == 'strict' %}
<div class="strict-shipping row section">
    <div class="col-md-12">
        <small class="shipping-cost">
            &#42;
            {% if shop.b2b %}
            {{ 'Excl. VAT' | t }}
            {% else %}
            {{ 'Incl. VAT' | t }}
            {% endif %}
            {{ 'Excl. $1' | t('<a href="' ~ 'service/shipping-returns' | url ~ '" target="_blank">' ~ 'Shipping costs' | t ~ '</a>') | raw }}
        </small>
    </div>
</div>
{% endif %}

<div id="home_lower_banner" class="section">
    <h2>{{ 'Great Collection' }}</h2>
    <div class="slider" data-cycle-auto-height="calc" data-cycle-progressive="#home_lower_banner_slides">
    {% for i in 1..3 %}
    {% if attribute(theme, 'homepage_lower_banner' ~ i ~ '_title') %}
    {% if i == 2 %}<script id="home_lower_banner_slides" type="text/cycle" data-cycle-split="---">{% endif %}
{% if i > 2 %}---{% endif %}
    <div>
        <div>
            <div class="img">
                <img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ ('homepage-lower-banner' ~ i ~ '-mobile.jpg') | url_asset }}" data-srcset="{{ ('homepage-lower-banner' ~ i ~ '-mobile.jpg') | url_asset }} 767w, {{ ('homepage-lower-banner' ~ i ~ '.jpg') | url_asset }} 1800w" data-sizes="100vw" alt="{{ 'homepage_lower_banner' ~ i }}" />
            </div>
            <div class="info">
                <h3>{{ attribute(theme, 'homepage_lower_banner' ~ i ~ '_title') | t }}</h3>
                <h4>{{ attribute(theme, 'homepage_lower_banner' ~ i ~ '_subtitle') | t }}</h4>
                {% if attribute(theme, 'homepage_lower_banner' ~ i ~ '_link') %}<a href="{{ attribute(theme, 'homepage_lower_banner' ~ i ~ '_link') }}" class="dark-btn">{{ 'More' | t }}</a>{% endif %}
            </div>
        </div>
    </div>
    {% endif %}
    {% endfor %}
    </script>
    <a href="#" class="advision-next cycle_pager"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17px" height="40px" viewBox="0 0 17 40" style="enable-background:new 0 0 17 40;" xml:space="preserve"><polygon points="0,40 0,35.3 13.1,20 0,4.7 0,0 17,20 "/></svg></a>
    <a href="#" class="advision-prev cycle_pager"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="17px" height="40px" viewBox="0 0 17 40" style="enable-background:new 0 0 17 40;" xml:space="preserve"><polygon points="0,40 0,35.3 13.1,20 0,4.7 0,0 17,20 "/></svg></a>
  </div>
</div>

<div id="social_plugins" class="section">
    <h2>{{ 'Social Feeds' | t }}</h2>
    <div id="plugins">
      {% if theme.googleplus_user %}
      <div id="google_plus" class="plugin">
        <label><svg viewBox="0 0 512 512"><path d="M179.7 237.6L179.7 284.2 256.7 284.2C253.6 304.2 233.4 342.9 179.7 342.9 133.4 342.9 95.6 304.4 95.6 257 95.6 209.6 133.4 171.1 179.7 171.1 206.1 171.1 223.7 182.4 233.8 192.1L270.6 156.6C247 134.4 216.4 121 179.7 121 104.7 121 44 181.8 44 257 44 332.2 104.7 393 179.7 393 258 393 310 337.8 310 260.1 310 251.2 309 244.4 307.9 237.6L179.7 237.6 179.7 237.6ZM468 236.7L429.3 236.7 429.3 198 390.7 198 390.7 236.7 352 236.7 352 275.3 390.7 275.3 390.7 314 429.3 314 429.3 275.3 468 275.3"></path></svg></label>
        <div class="google-plus-activity" data-options="{ user: '{{ theme.googleplus_user }}' }"></div>
        <script>
            var gplus_loaded = false;
          $(window).load(function(){
              if(!gplus_loaded){
                  gplus_loaded = true;
                $.getScript( "{{ 'jquery-googleplus-activity-1-0-prod.js' | url_asset }}", function(){
                  jQuery.fn.googlePlusActivity.defaults.api_key = 'AIzaSyA8RhFJlyTVYsoR1-qWnHBQeGGTulRiNNY';
                });
              }
          });
          </script>
        </div>
        {% endif %}
        {% if theme.twitter_timeline_url %}
        <div id="twitter_timeline" class="plugin">
            <label>
                <svg viewBox="0 0 512 512"><path d="M419.6 168.6c-11.7 5.2-24.2 8.7-37.4 10.2 13.4-8.1 23.8-20.8 28.6-36 -12.6 7.5-26.5 12.9-41.3 15.8 -11.9-12.6-28.8-20.6-47.5-20.6 -42 0-72.9 39.2-63.4 79.9 -54.1-2.7-102.1-28.6-134.2-68 -17 29.2-8.8 67.5 20.1 86.9 -10.7-0.3-20.7-3.3-29.5-8.1 -0.7 30.2 20.9 58.4 52.2 64.6 -9.2 2.5-19.2 3.1-29.4 1.1 8.3 25.9 32.3 44.7 60.8 45.2 -27.4 21.4-61.8 31-96.4 27 28.8 18.5 63 29.2 99.8 29.2 120.8 0 189.1-102.1 185-193.6C399.9 193.1 410.9 181.7 419.6 168.6z"></path></svg>
                {{ 'Tweets by' | t }} @{{ theme.twitter_username }}
            </label>
            <a class="twitter-timeline"  href="{{ theme.twitter_timeline_url }}"  data-widget-id="{{ theme.twitter_timeline_widgetid }}" data-theme="light" style="height:150px" data-tweet-limit="2" data-chrome="nofooter noheader noborders noscrollbar transparent" data-link-color="{{ theme.highlight_color }}">{{ 'Tweets by' | t }} @{{ theme.twitter_username }}</a>
            <script>
                $(window).load(function(){
                    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
                });
            </script>
        </div>
        {% endif %}
        {% if theme.facebook_url %}
        <div id="facebook_timeline" class="plugin">
            <label>
                <svg viewBox="0 0 512 512"><path d="M211.9 197.4h-36.7v59.9h36.7V433.1h70.5V256.5h49.2l5.2-59.1h-54.4c0 0 0-22.1 0-33.7 0-13.9 2.8-19.5 16.3-19.5 10.9 0 38.2 0 38.2 0V82.9c0 0-40.2 0-48.8 0 -52.5 0-76.1 23.1-76.1 67.3C211.9 188.8 211.9 197.4 211.9 197.4z"></path></svg>
            </label>
            <div id="fb-root"></div>
            <div class="fb-page" data-href="{{ theme.facebook_url }}" data-tabs="timeline" data-small-header="false" data-width="500" data-height="252" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>
            <script>
                var facebook_container = $('#facebook_timeline').width();
                $(window).load(function(){
                    (function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "https://connect.facebook.net/en_US/all.js#xfbml=1";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                });

                $(window).resize(function() {
                    if($('#facebook_timeline').width() != facebook_container){
                        facebook_container = $('#facebook_timeline').width();
                        $('.fb-page').replaceWith('<div class="fb-page" data-href="{{ theme.facebook_url }}" data-tabs="timeline" data-small-header="false" data-width="500" data-height="252" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/facebook"><a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>');
                        FB.XFBML.parse();  }
                });</script>
        </div>
        {% endif %}

        {% if shop.blogs | length > 0 %}
        {% set article_posted = '' %}
        <div id="blog_plugin" class="plugin">
            {% for blog in shop.blogs %}
            {% for article in blog.articles %}
            {% if loop.first %}
            {% set article_posted = article.date %}
            {% endif %}
            <a href="{{ article.url | url }}" title="{{ article.title }}">
                <div class="img">
                    <img class="lazyload" alt="{{ article.title }}" src="{{ 'blank.gif' | url_asset }}" data-src="{{ article.image | url_image('122x60x1', article.title ) }}" />
                </div>
                <div class="info">
                    <span>{{ article.title }}</span>
                    <p>{{ article.summary | raw }}</p>
                </div>
            </a>
            {% endfor %}
            {% endfor %}
            <label><img class="lazyload" alt="blog logo" src="{{ 'blank.gif' | url_asset }}" data-src="{{ 'blog-logo-mini.png' | url_asset }}" /><span>{{ 'Last Post' | t }} {{ article_posted | date("M jS, Y") }}</span></label>
        </div>
        {% endif %}
    </div>
</div>

<div id="homepage-testimonials" class="section">
    <h2>{{ 'Testimonials' | t }}</h2>
    <div data-cycle-auto-height="calc" {#data-cycle-progressive="#testimonial-slides"#}>
        {% for i in 1..3 %}
        {% if attribute(theme, 'testimonial' ~ i ~ '_quote') %}
        {#{% if i == 2 %}<script id="testimonial-slides" type="text/cycle" data-cycle-split="---">{% endif %}
        {% if i > 2 %}---{% endif %}#}
        <div class="slide">
            <div>
              <div class="img"><img class="lazyload" src="{{ 'blank.gif' | url_asset }}" data-src="{{ ('testimonial-' ~ i ~ '.jpg') | url_asset }}" alt="{{ 'testimonial' ~ i ~'_name'}}" /></div>
              <blockquote>{{ attribute(theme, 'testimonial' ~ i ~ '_quote') | t }}</blockquote>
              {% if attribute(theme, 'testimonial' ~ i ~ '_link') %}<p><a href="{{ attribute(theme, 'testimonial' ~ i ~ '_link') }}" title="{{ 'Read More' | t }}">{{ 'Read More' | t }}</a></p>{% endif %}
            </div>
        </div>
        {% endif %}
        {% endfor %}
        {#</script>#}
        <div class="cycle-pager"></div>
    </div>
</div>

{% if content != '' %}
<div id="homepage-text" class="section">{{ content | raw }}</div>
{% endif %}

<div id="homepage-map" class="map">
    <div>
        <h2>{{ 'Nearest Location' | t }}</h2>
    </div>
    <iframe src="https://www.google.com/maps/d/u/0/embed?mid=19lFapUWQgQFo4pBOvV6gM7s44Dg&ll=36.031331795163645%2C-88.48388668125&z=5" width="640" height="480"></iframe>
</div>
<div class="container content">