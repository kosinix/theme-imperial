jQuery(document).ready(function($){

    // Assign selectors once
    var $window = $(window),
    	$doc = $(document),
        $stickyNav = $('#sticky-nav'),
		$hamburger = $('#hamburger'),
		$menuSide = $("#menu-side"),
		$nice = null;

    $window.scroll(function(){
        if($window.scrollTop() > 98){
            $stickyNav.addClass('sticky');
			if($hamburger.hasClass('active')){
				$hamburger.trigger('click');
			}
        } else {
            $stickyNav.removeClass('sticky');
        }
    });

	$menuSide.niceScroll({
		autohidemode: true,
		cursorcolor: "#fff",
		cursorborder: 0,
		cursoropacitymin: 0.5,
		cursorborderradius:"5px",
		cursorwidth: "8px",
		railpadding: {top:10, bottom:10, left:10, right:10},
	});
	$nice = $('#ascrail2000');

	$hamburger.on('click', function(e){
		
		if($hamburger.hasClass('active')){
			$hamburger.removeClass('active');
			$menuSide.removeClass('show');
			$nice.removeClass('show');
		} else {
			$hamburger.addClass('active');
			$menuSide.addClass('show');
			$nice.addClass('show');
		}
	});
});

// Limit exec of taxing function
// https://davidwalsh.name/javascript-debounce-function
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};