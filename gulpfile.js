var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var runSequence = require('run-sequence'); // Some tasks require specific order

// Convert SASS to CSS 
gulp.task('sass', function () {
    return gulp.src('sass/**/*.scss')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(gulp.dest('css/src'));
});

// CSS concat
gulp.task('css-concat', function() {
    var cssIn = [
        'css/src/bootstrap.css', // Add bootstrap first
        'css/src/**.*css',
    ]; // Use array for specific order
    var cssOutPath = 'css';
    return gulp.src(cssIn, {base: 'css/src'})
        .pipe(concat('style.css'))
        .pipe(gulp.dest(cssOutPath));
});

// Minify CSS
gulp.task('css-min', function () {
    var src = [
        '!css/*.min.css', // Ignore min files
        'css/*.css'
    ]
    return gulp.src(src, {base: 'css'})
        .pipe(sourcemaps.init())
        .pipe(cleanCSS({
            level: {
                1: {
                    specialComments: false // Remove all comments
                },
                2:{}
            }
        }))
        .pipe(rename(function (path) {
            path.extname = ".min" + path.extname;
        }))
        .pipe(sourcemaps.write('.')) // Write sourcemaps to external files at the same dir. Remove param to inline maps.
        .pipe(gulp.dest('D:/webserver/htdocs/tmp/imp/css'))
        .pipe(gulp.dest('css'));
});

// CSS
gulp.task('css', function(callback) {
  runSequence('sass', 'css-concat', 'css-min', callback);
}); // Runs on "gulp css"

// JS Concat
gulp.task('js-concat', function() {
    var jsIn = [
        'js/src/jquery-1.*.js', // Add jquery 1.x first before jquery plugins and scripts
        'js/src/*.js'
    ];
    var jsOutPath = 'js';
    return gulp.src(jsIn, {base: '.'})
        .pipe(concat('script.js'))
        .pipe(gulp.dest(jsOutPath));
});

// Minify JS
gulp.task('js-min', function() {
    var src = [
        '!js/*.min.js', // Ignore min files
        'js/*.js'
    ]
    var out = 'js';
    return gulp.src(src, {base: 'css'})
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename(function (path) {
            path.extname = ".min" + path.extname;
        }))
        .pipe(sourcemaps.write('.')) // Write sourcemaps to external files at the same dir. Remove param to inline.
        .pipe(gulp.dest('D:/webserver/htdocs/tmp/imp/js'))
        .pipe(gulp.dest(out));
});

// JS
gulp.task('js', function(callback) {
  runSequence('js-concat', 'js-min', callback);
}); // Runs on "gulp js"

// Gulp
gulp.task('default', ['css', 'js']); // Runs on "gulp" command only

// Watch over me...
gulp.task('watch', function(){
    gulp.watch('sass/*.scss', ['css']).on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });

    gulp.watch('js/src/*.js', ['js']).on('change', function(event) {
        console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

